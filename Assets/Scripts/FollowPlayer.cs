﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {
	private Transform player;
	private Vector3 offset;

	void Start() {
		player = GameObject.Find("Player").transform;
		offset.Set(0, 1, -5);
	}
	
	void FixedUpdate() {
		transform.position = player.position + offset;
	}
}
