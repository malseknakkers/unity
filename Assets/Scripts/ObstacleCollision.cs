﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollision : MonoBehaviour {

	void Start () {
		
	}

	void OnCollisionEnter(Collision collisioninfo) {
		if(collisioninfo.collider.tag == "Player") {
			GetComponent<Rigidbody>().useGravity = false;
		}
	}
}
