﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	private Rigidbody rb;
	private float forwardForce;
	private float sidewaysForce;

	void Start() {
		rb = GetComponent<Rigidbody>();
		forwardForce = 2000f;
		sidewaysForce = 5f;
	}
	
	// Change player input
	void FixedUpdate() {
		rb.AddForce(0, 0, forwardForce * Time.deltaTime);
		if(Input.GetKey("d")) {
			transform.Translate(Vector3.right * sidewaysForce * Input.GetAxis("Horizontal") * Time.deltaTime);
		}

		if(Input.GetKey("a")) {
			transform.Translate(-Vector3.left * sidewaysForce * Input.GetAxis("Horizontal") * Time.deltaTime);
		}
	}
}
