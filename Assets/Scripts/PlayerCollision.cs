﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour {

	void Start () {
		
	}

	void OnCollisionEnter(Collision collisioninfo) {
		if(collisioninfo.collider.tag == "Obstacle") {
            GetComponent<PlayerMovement>().enabled = false;
		}
	}
}
